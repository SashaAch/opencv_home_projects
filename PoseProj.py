import cv2 as cv
import time as t
import PoseEstimationModule as pem
import HandTrachingModule as htm

cap = cv.VideoCapture(0)
pTime = 0
pose_detector = pem.poseDetector()
hand_detector = htm.handDetector()

while True:
    success, img = cap.read()
    img = cv.flip(img, flipCode=1)

    img = pose_detector.findPose(img)
    lmList_pose = pose_detector.findPosition(img)

    img = hand_detector.findHands(img)
    lmList_hand = hand_detector.findHandPosition(img)

    # print(lmList)
    # cv.circle(img, (lmList[14][1], lmList[14][2]), 10, (255, 0, 255), cv.FILLED)

    cTime = t.time()
    fps = 1/(cTime-pTime)
    pTime = cTime

    cv.putText(img,str(int(fps)), (70, 50),
               cv.FONT_HERSHEY_PLAIN, 3, (255,0,0), 3)


    cv.imshow("Image", img)
    cv.waitKey(1)