import cv2 as cv
import mediapipe as mp
import time as t

cap = cv.VideoCapture(0)
mpFaceDetect = mp.solutions.face_detection
pTime = 0
mpDraw = mp.solutions.drawing_utils
faceDetect = mpFaceDetect.FaceDetection(0.75) # 0.5


while True:

    success, img = cap.read()
    img = cv.flip(img, flipCode=1)
    imgRGB = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    results = faceDetect.process(imgRGB)

    if results.detections:
        for id, detection in enumerate(results.detections):
            # mpDraw.draw_detection(img, detection)

            bBox = detection.location_data.relative_bounding_box
            h, w, c = img.shape
            x_y_minBox = int(bBox.xmin * w), int(bBox.ymin * h),\
                         int(bBox.width * w),int(bBox.height * h)
            cv.rectangle(img, x_y_minBox, (255, 0 ,255), 2)
            cv.putText(img, f'{int(detection.score[0] * 100)}%', (x_y_minBox[0], x_y_minBox[1] - 20), cv.FONT_HERSHEY_PLAIN,
                       2, (255, 0, 255), 2)

    cTime = t.time()
    fps = 1/(cTime - pTime)
    pTime = cTime
    cv.putText(img, f'FPS :{int(fps)}', (20,70), cv.FONT_HERSHEY_PLAIN,
               2, (255, 0, 0), 2)





    cv.imshow('Image', img)
    cv.waitKey(1)
