import cv2 as cv
import mediapipe as mp
import time as t

class FaceDetector():
    def __init__(self, minDetectionConf=0.5):
        self.minDetectionConf = minDetectionConf
        self.mpFaceDetections = mp.solutions.face_detection

        self.mpDraw = mp.solutions.drawing_utils
        self.faceDetection = self.mpFaceDetections.FaceDetection(self.minDetectionConf) # 0.5

    def findFaces(self, img, draw=True):

        imgRGB = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        self.results = self.faceDetection.process(imgRGB)
        bboxs = []
        if self.results.detections:
            for id, detection in enumerate(self.results.detections):
                # mpDraw.draw_detection(img, detection)
                bBox = detection.location_data.relative_bounding_box
                h, w, c = img.shape
                x_y_minBox = int(bBox.xmin * w), int(bBox.ymin * h),\
                             int(bBox.width * w),int(bBox.height * h)
                bboxs.append([id, x_y_minBox, detection.score])
                if draw:
                    
                    img = self.fancyDraw(img, x_y_minBox)
                    cv.putText(img, f'{int(detection.score[0] * 100)}%', (x_y_minBox[0], x_y_minBox[1] - 20), cv.FONT_HERSHEY_PLAIN,
                               2, (255, 0, 255), 2)
        return img, bboxs

    def fancyDraw(self, img, bbox, l = 30, k = 5, rt = 1):
        x, y, w, h = bbox
        x1, y1 = x + w, y + h

        cv.rectangle(img, bbox, (255, 0, 255), rt)
        # Top left
        cv.line(img, (x,y), (x+l, y), (255, 0, 255), k)
        cv.line(img, (x, y), (x, y+l), (255, 0, 255), k)
        # Top Right
        cv.line(img, (x1, y), (x1 - l, y), (255, 0, 255), k)
        cv.line(img, (x1, y), (x1, y + l), (255, 0, 255), k)
        # Bottom left
        cv.line(img, (x, y1), (x + l, y1), (255, 0, 255), k)
        cv.line(img, (x, y1), (x, y1 - l), (255, 0, 255), k)
        # Bottom Right
        cv.line(img, (x1, y1), (x1 - l, y1), (255, 0, 255), k)
        cv.line(img, (x1, y1), (x1, y1 - l), (255, 0, 255), k)

        return img

def main():
    cap = cv.VideoCapture(0)
    pTime = 0
    detector = FaceDetector()
    while True:
        success, img = cap.read()
        img = cv.flip(img, flipCode=1)
        img, bboxs = detector.findFaces(img)




        cTime = t.time()
        fps = 1 / (cTime - pTime)
        pTime = cTime
        cv.putText(img, f'FPS :{int(fps)}', (20, 70), cv.FONT_HERSHEY_PLAIN,
                   2, (255, 0, 0), 2)

        cv.imshow('Image', img)
        cv.waitKey(1)

if __name__ == "__main__":
    main()