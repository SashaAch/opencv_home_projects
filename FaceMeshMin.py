import cv2 as cv
import mediapipe as mp
import time as t

cap = cv.VideoCapture(0)
mpFaceMesh = mp.solutions.face_mesh
mpDraw = mp.solutions.drawing_utils
pTime = 0
FaceMeshMin = mpFaceMesh.FaceMesh()
drawSpec = mpDraw.DrawingSpec(thickness=1, circle_radius=1, color=(255, 0, 255))

while True:
    success, img = cap.read()
    img = cv.flip(img, flipCode=1)
    imgRGB = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    results = FaceMeshMin.process(imgRGB)

    if results.multi_face_landmarks:
        for mesh in enumerate(results.multi_face_landmarks):
            mpDraw.draw_landmarks(img, mesh, mpFaceMesh.FACEMESH_TESSELATION, drawSpec, drawSpec)
            for lm in mesh.landmark:
                h, w, c = img.shape
                x, y = int(lm.x * w), int(lm.y * h)
                print(x, y)

    cTime = t.time()
    fps = 1/(cTime - pTime)
    pTime = cTime
    cv.putText(img, f'FPS :{int(fps)}', (20, 70), cv.FONT_HERSHEY_PLAIN,
               2, (255, 0, 0), 2)

    cv.imshow('Face Mesh min', img)
    cv.waitKey(1)
