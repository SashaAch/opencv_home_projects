import cv2 as cv
import mediapipe as mp
import time as t

class poseDetector():

    def __init__(self, mode=False, upBody=False, smooth=True,
                 detectionConf=0.5, trackConf=0.5):

        self.mode = mode
        self.upBody = upBody
        self.smooth = smooth
        self.detectionConf = detectionConf
        self.trackConf = trackConf

        self.mpDraw = mp.solutions.drawing_utils
        self.mpPose = mp.solutions.pose
        self.pose = self.mpPose.Pose()

    def findPose(self, img, draw=True):

        imgRGB = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        self.results = self.pose.process(imgRGB)

        if self.results.pose_landmarks:
            if draw:
                self.mpDraw.draw_landmarks(img, self.results.pose_landmarks, self.mpPose.POSE_CONNECTIONS)

        return img

    def findPosition(self, img, draw=True):

        lmList = []

        if self.results.pose_landmarks:
            for id, lm in enumerate(self.results.pose_landmarks.landmark):
                h, w, c = img.shape
                cx, cy = int(lm.x * w), int(lm.y * h)
                lmList.append([id, cx, cy])
                if draw:
                    cv.circle(img, (cx, cy), 10, (255, 0, 0), cv.FILLED)
        return lmList

def main():
    cap = cv.VideoCapture(0)
    pTime = 0
    detector = poseDetector()

    while True:
        success, img = cap.read()
        img = cv.flip(img, flipCode=1)
        img = detector.findPose(img)
        lmList = detector.findPosition(img)
        # print(lmList)
        cv.circle(img, (lmList[14][1], lmList[14][2]), 10, (255, 0, 255), cv.FILLED)

        cTime = t.time()
        fps = 1/(cTime-pTime)
        pTime = cTime

        cv.putText(img,str(int(fps)), (70, 50),
                   cv.FONT_HERSHEY_PLAIN, 3, (255,0,0), 3)


        cv.imshow("Image", img)
        cv.waitKey(1)


if __name__ == "__main__":
    main()